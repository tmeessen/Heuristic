//
// Created by tmeessen on 7/28/18.
//


#include <algorithm>
#include <vector>

#include "CH3.h"

using namespace std;
Solution* CH3Solver::solve(problem *p) {
    // Empty solution
    auto s = new Solution(p);
    s->solver_desc = "CH3 --- Toyoda";

    auto ignored = vector<int>(p->n, 0);


    // Iterations
    bool item_added;
    int item_to_add;
    do {
        item_to_add = get_next_candidate(s->sol, p, &ignored[0]);
        if(item_to_add >= 0) {
            item_added = s->check_and_add_item(item_to_add, p) == 0;
            if (!item_added)
                ignored[item_to_add] = 1; // That item will be skipped
        }
    }while(item_to_add >= 0);

    return s;
}


int CH3Solver::get_first(int n, int m, int *profits, float **constraints) {
    // Total demand by item
    auto tot_by_item = vector<float>(n);
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            tot_by_item[j] += constraints[i][j];
        }
    }

    // Pseudo-utility for each item
    auto pseudo_utility = vector<float>(n);
    for (int j = 0; j < n; j++) {
        pseudo_utility[j] = profits[j] / tot_by_item[j];
    }


    // Retrieving item with maximum pseudo-utility
    auto max = max_element(pseudo_utility.begin(), pseudo_utility.end());
    int position = (int)distance(pseudo_utility.begin(), max);

    return position;
}

int CH3Solver::get_best(int n, int m, int *profits, float **constraints, int const *items, int const *ignored) {
    // Determine current resources usage by constraint [Ui]
    auto usage = vector<float>(m);
    for(int i = 0; i < m; i++) {
        usage[i] = 0;
        for(int j = 0; j < n; j++) {
            usage[i] += items[j] * constraints[i][j];
        }
    }
    // Norm the resources vector [||Ui||]
    double norm = 0;
    for(int i = 0; i < m; i++) {
        norm += pow(usage[i], 2);
    }
    norm = sqrt(norm);
    if (norm != 0) {
        for(int i = 0; i < m; i++) {
            usage[i] /= norm;
        }
    }
    // pseudo-weight by items [Vj]
    auto pseudo_weight = vector<float>(n);
    for(int j = 0; j < n; j++) {
        // for each item
        pseudo_weight[j] = 0;
        for(int i = 0; i < m; i++) {
            // for each constraint
            pseudo_weight[j] += constraints[i][j] * usage[i];
        }
    }

    // pseudo-utility by item
    auto pseudo_utility = vector<float>(n);
    for(int j = 0; j < n; j++) {
        pseudo_utility[j] = profits[j] / pseudo_weight[j];
        // ignoring some items
        pseudo_utility[j] *= items[j] + ignored[j] == 0 ? 1 : -1;
    }

    // Retrieving item with maximum pseudo-utility
    auto max = max_element(pseudo_utility.begin(), pseudo_utility.end());
    int position = (int)distance(pseudo_utility.begin(), max);
    if (pseudo_utility[position] >= 0) {
        return position;
    } else {
        // No items are available
        return -1;
    }
}

//
// Transform the constraints of a mkp problem to a normalized form. Where capacities for each constraint is 1.
//
float** CH3Solver::norm_constraints(int n, int m, int **constraints, int* capacities) {
    auto constraints_normed = new float*[m];
    for (int i = 0 ; i < m ; i++) {
        constraints_normed[i] = new float[n];
        for(int j = 0; j < n; j++) {
            constraints_normed[i][j] = constraints[i][j];
        }
    }

    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++)
            constraints_normed[i][j] /= capacities[i];
    }

    return constraints_normed;
}


int CH3Solver::get_next_candidate(int const* items_picked, problem* p, int const *items_ignored) {

    float **constraints_normed = norm_constraints(p->n, p->m, p->constraints, p->capacities);

    int sum = 0;
    int best_item;
    for(int j = 0; j < p->n; j++) {
        // counting already picked items
        sum += items_picked[j];
    }

    if (sum == 0) {
        best_item = get_first(p->n, p->m, p->profits, constraints_normed);
    } else if(sum == p->n){
        // No more items
        best_item = -1;
    } else {
        best_item = get_best(p->n,p->m, p->profits, constraints_normed, items_picked, items_ignored);
    }

    // Cleaning
    for(int i = 0; i < p->m; i++) {
        delete[](constraints_normed[i]);
    }
    delete[](constraints_normed);
    return best_item;
}