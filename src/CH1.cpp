//
// Created by tmeessen on 7/28/18.
//

#include "CH1.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include "util.h"

using namespace std;

int CH1Solver::get_next_candidate(int const*items_picked, problem* p, int const *items_ignored) {
    // Generate available items
    auto choice = vector<int>();
    for (int j = 0; j < p->n; j++) {
        if(items_ignored[j] == 0 && items_picked[j] == 0 ) {
            choice.push_back(j);
        }
    }
    // Pick one of them at random
    auto item_available_count = distance(choice.begin(), choice.end());
    if (item_available_count == 0)
        return -1;
    else
        return choice[rand() % item_available_count ];
}

Solution* CH1Solver::solve(problem *p) {

    int sum = 0;
    // Solve SAMPLE_SIZE time the problem with a random method
    for (int i = 0; i < SAMPLE_SIZE; i++) {
        auto* s = new Solution(p);
        auto items_ignored = vector<int>(p->n,0);
        int item_best;
        do{
            // While there are still potential candidates
            item_best = get_next_candidate(s->sol, p, &items_ignored[0]);
            s->check_and_add_item(item_best, p);
            items_ignored [item_best] = 1;
            item_best = get_next_candidate(s->sol, p, &items_ignored[0]);
        }while (item_best >= 0) ;
        sum += s->value;
    }
    // Average of the obtained values
    auto s = new CH1Solution(p, sum/SAMPLE_SIZE);
    s->solver_desc = "Sample of randomly constructed solution.";
    return s;
}


CH1Solution::CH1Solution(problem *p, int avg): Solution(p) {
    average_profit = avg;
}

void CH1Solution::print() const {
    cout<<"Solver:\n" << solver_desc << endl;
    cout<<"Average value " << average_profit << endl;
}