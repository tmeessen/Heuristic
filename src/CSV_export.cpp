//
// Created by tmeessen on 7/30/18.
//

#include <iostream>
#include <fstream>

#include "CSV_export.h"


using namespace std;

CSV_export::CSV_export(char *logfile_) {
    log_file = logfile_;
    ofstream f_stream(log_file, ofstream::trunc);
    f_stream << "problem_name, solver_name, value" << endl;
}

void CSV_export::print(char *problem_name, int value, SolverName solver) {
    ofstream f_stream(log_file, ofstream::app);
    string solver_name;
    switch(solver) {
        case SolverName::CH1:
            solver_name = "CH1";
            break;
        case SolverName::CH2:
            solver_name = "CH2";
            break;
        case SolverName::CH3:
            solver_name = "CH3";
            break;
    }
    f_stream <<  problem_name <<","<< solver_name <<","<< value << endl;
}

void CSV_export::time_log(double time) {
    string time_log = log_file + "_time";
    ofstream f_stream(time_log, ofstream::trunc);
    f_stream << time << endl;
}