/***************************************************************************
 *   Copyright (C) 2018 Alberto Franzin                                    *
 *   alberto.franzin@ulb.ac.be                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <cstring>
#include <iostream>

#include "mkpsolution.h"

using namespace std;

Solution::Solution(problem *p) {
  n = p->n;
  m = p->m;

  sol = (int *)calloc(n, sizeof(int));
  value = 0;

  num_selected = 0;
  num_discarded = n;

  num_violated = 0;
  violated = (int *)calloc(m, sizeof(int));
  resources_used = (int *)calloc(m, sizeof(int));
}

Solution::Solution() {
  n = 0;
  m = 0;
  value = 0;
  sol = nullptr;
  num_selected = 0;
  num_discarded = 0;
  resources_used = nullptr;
  violated = nullptr;
  num_violated = 0;
}

Solution::Solution(const Solution &s) {
    n = s.n;
    m = s.m;
    num_selected = s.num_selected;
    num_discarded = s.num_discarded;
    value = s.value;
    num_violated = s.num_violated;
    sol = new int[n];
    for (int i = 0 ; i < n ; i++) {
        sol[i] = s.sol[i];
    }
    violated = new int[m];
    resources_used = new int[m];
    for (int i = 0 ; i < m ; i++) {
        violated[i] = s.violated[i];
        resources_used[i] = s.resources_used[i];
    }
}

Solution::~Solution() {
  if (sol != nullptr) free(sol);
  if (violated != nullptr) free(violated);
  if (resources_used != nullptr) free(resources_used);
}

void Solution::print() const{
  cout<<"Solver:\n" << solver_desc << endl;
  if (num_selected == 0) cout<< "No items selected in the solution!\n";
  else {
    cout << "Items in solution:\n[ ";
    for (int i = 0 ; i < n ; i++) {
      if (sol[i] == 1) cout << i << " ";
    }
    cout << "]\nSolution value: " << value << endl;
  }

  if (num_violated == 0) {
    cout << "This is a feasible solution." << endl;
  } else {
    printf("Violated constraints:\n");
    for (int i = 0 ; i < m ; i++) {
       if (violated[i] == 1) printf("%d ",i);
    }
    printf("\n");
  }

}

void Solution::remove_item(int item, problem *p) {
    if (sol[item] == 1) {
        sol[item] = 0;
        value -= p->profits[item];
        num_selected--;
        num_discarded++;

        for (int i = 0 ; i < m ; i++) {
            resources_used[i] -= p->constraints[i][item];
        }
    }
}


int Solution::check_and_add_item(int item, problem *p) {
  int i, v = 0;
  // check whether adding the item would lead to violate some contraints
  // and count how many
  for (i = 0 ; i < m ; i++) {
    if (resources_used[i] + p->constraints[i][item] > p->capacities[i]) {
      v++;
    }
  }
  // if no constraint is violated, add the item to the solution
  if (v == 0) {
    add_item( item, p);
  }
  return(v);
}

void Solution::add_item(int item, problem *p) {
  // if item was already in the solution, do nothing

  if (sol[item] == 0) {
    sol[item] = 1;
    num_selected++;
    num_discarded--;
    value += p->profits[item]; // delta evaluation

    // update list of resources used by the current solution
    for (int i = 0 ; i < m ; i++) {
      resources_used[i] += p->constraints[i][item];
    }
  }
}
