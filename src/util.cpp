/***************************************************************************
 *   Copyright (C) 2018 Alberto Franzin                                    *
 *   alberto.franzin@ulb.ac.be                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <cstring>
#include "util.h"
#include "CH2.h"
#include "CH1.h"
#include "CH3.h"
#include "FI.h"



using namespace std;


int *create_shuffled(int n) {
  int i,
      *v = (int *)malloc(n * sizeof(int));
  for (i = 0 ; i < n ; i++) v[i] = i;
  
  shuffle_int(v, n);
  
  return(v);
}

void shuffle_int(int *v, int n) {
  int i,  j, tmp;
  for (i = n-1 ; i >= 1 ; i--) {
    j = rand() % i;
    tmp = v[i];
    v[i] = v[j];
    v[j] = tmp;
  }
}




Params::Params(int argc, char **argv) {
    int i;
    file_read = 0;
    instance_files = vector<string>();
    solver = SolverName::CH3;
    move_type = Move_type::None;
    seed = 0;
    bool expecting_filename = false;
    for (i = 1 ; i < argc ; i++) {
        if (strcmp(argv[i], "--seed") == 0) {
            i++;
            seed = atoi(argv[i]);
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--CH2") == 0) {
            solver = SolverName::CH2;
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--CH1") == 0) {
            solver = SolverName::CH1;
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--CH3") == 0) {
            solver = SolverName::CH3;
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--output") == 0) {
            i++;
            log_file = argv[i];
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--FI") == 0) {
            move_type = Move_type::FI;
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--BI") == 0) {
            move_type = Move_type::BI;
            expecting_filename = false;
        }
        if (strcmp(argv[i], "--files") == 0) {
            expecting_filename = true;
            i++;
        }
        if (expecting_filename) {
            instance_files.emplace_back(argv[i]);
        }
    }
}

char* Params::get_next_filename() {
    if (instance_files.capacity() > file_read && instance_files[file_read] != "") {
        string filename = instance_files[file_read];
        file_read ++;
        // String to char* conversion
        auto *cstr = new char[filename.length() + 1];
        strcpy(cstr, filename.c_str());
        return cstr;
    } else {
        return nullptr;
    }

}


Solver* getSolver (SolverName solver_name, Move_type move_type) {
    Solver* s;
    switch (move_type) {
        case Move_type::None: {
            switch (solver_name) {
                case SolverName::CH2: {
                    s = new CH2Solver;
                    break;
                }
                case SolverName::CH1: {
                    s = new CH1Solver;
                    break;
                }
                case SolverName::CH3: {
                    s = new CH3Solver;
                    break;
                }
            }
            break;
        }
        case Move_type::FI : {
            switch (solver_name) {
                case SolverName::CH2: {
                    auto s_init = new CH2Solver;
                    s = new FI(s_init);
                    break;
                }
                case SolverName::CH1: {
                    auto s_init = new CH1Solver;
                    s = new FI(s_init);
                    break;
                }
                case SolverName ::CH3: {
                    auto s_init = new CH3Solver;
                    s = new FI(s_init);
                    break;
                }
            }
            break;
        }
    }

    return s;
}

