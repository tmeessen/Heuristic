/***************************************************************************
 *   Copyright (C) 2018 Alberto Franzin                                    *
 *   alberto.franzin@ulb.ac.be                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cstdio>
#include <cmath>
#include <climits>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <ctime>

#include "mkpdata.h"
#include "util.h"
#include "mkpio.h"
#include "mkpsolution.h"
#include "mkpproblem.h"
#include "Solver.h"
#include "CSV_export.h"

int main(int argc, char *argv[]) {

  Params pars(argc, argv);

  CSV_export out(pars.get_log_file());
  pars.set_seed();
  Solver *solver = getSolver(pars.get_solver_name(), pars.get_solver_move());

  char *filename = pars.get_next_filename();
  clock_t start, end;
  start = clock();


  while(filename != nullptr) {

      problem *p = read_problem(filename);
      // Initial solution
      Solution *s = solver->solve(p);
      out.print(filename, s->get_profit(), pars.get_solver_name());
      destroy_problem(p);
      filename = pars.get_next_filename();
  }
  end = clock();
  out.time_log((end-start)*1000/CLOCKS_PER_SEC); // Time in millisecond


  return(0);
  
}

