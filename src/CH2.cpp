//
// Created by tmeessen on 7/27/18.
//
#include <vector>
#include <algorithm>

#include "CH2.h"

using namespace std;
Solution* CH2Solver::solve(problem *p) {
    // Empty solution
    auto s = new Solution(p);
    s->solver_desc = "CH2 --- Greedy ";

    auto candidates_item = vector<int>(p->n, 0);
    int best_candidate = get_next_candidate(s->sol, p, &candidates_item[0]);
    while(best_candidate >= 0) {
        s->check_and_add_item(best_candidate, p);
        // Not picking the same item twice
        candidates_item [best_candidate] = 1;
        best_candidate = get_next_candidate(s->sol, p, &candidates_item[0]);
    }
    return s;
}


int CH2Solver::get_next_candidate(int const*items_picked, problem* p, int const *items_ignored) {
    int n = p->n;
    auto profits_copy = vector<int>(n);
    for (int j = 0; j < n ; j++) {
        profits_copy[j] = p->profits[j];
    }
    for(int j = 0; j < n; j++) {
        // Ignoring items that are not available
        if(items_ignored[j] != 0)
            profits_copy[j] = -1;
    }
    auto max = max_element(profits_copy.begin(), profits_copy.end());
    long position = distance( profits_copy.begin(), max);
    if (profits_copy[position] < 0) {
        return -1;
    } else {
        return (int)position;
    }
}