//
// Created by tmeessen on 7/31/18.
//

#include <vector>

#include "FI.h"
#include "util.h"

using namespace std;

FI::FI(Solver *s) {
    initial_solver = s;
}

Solution* FI::do_a_move(Solution *s, problem* p, int *items_to_remove, int k) {
    Solution* new_s = new Solution(*s);
    // Remove selected items
    auto items_ignored = vector<int>(p->n, 0);
    for(int i = 0; i < k; i++) {
        new_s->remove_item(items_to_remove[i], p);
        items_ignored[items_to_remove[i]] = 1;
    }

    // Construct updated solution
    int item_to_add;
    do{
        item_to_add = initial_solver->get_next_candidate(new_s->sol, p, &items_ignored[0]);
        new_s->check_and_add_item(item_to_add, p);
        // Ignore the item for further iteration anyway.
        items_ignored[item_to_add] = 1;
    } while (item_to_add >= 0);

    return new_s;
}

Solution* FI::solve(problem *p) {
    // Initial solution
    Solution * s = initial_solver->solve(p);
    Solution * new_s = nullptr;
    auto candidates_for_elimination = vector<int>();
    candidates_for_elimination.reserve(s->num_selected);
    for(int j = 0; j < p->n; j++) {
        if (s->sol[j] == 1)
            candidates_for_elimination.push_back(j);
    }
    int number_of_candidates = (int)distance(candidates_for_elimination.begin(), candidates_for_elimination.end());
    shuffle_int(&candidates_for_elimination[0], number_of_candidates);
    int profit_from_move = 0;
    int counter_move_made = 0;
    do {
        new_s = do_a_move(s, p, &candidates_for_elimination[counter_move_made], 1);
        profit_from_move = new_s->value - s->value;
        counter_move_made ++;
    }while (profit_from_move <= 0 && counter_move_made < p->n);
    return profit_from_move > 0? new_s : s;
}