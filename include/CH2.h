//
// Created by tmeessen on 7/27/18.
//

#ifndef HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH2SOLVER_H
#define HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH2SOLVER_H

#include "Solver.h"

class CH2Solver: public Solver {

public:
    /**
     * Use the profit value to find the best candidate among the available items
     * @param p Problem
     * @param items_picked Vector, items with 1 are already in solution
     * @return Best greedy item
     */
    int get_next_candidate(int const* items_picked, problem* p, int const *items_ignored) override;
    Solution* solve(problem *p) override;

};


#endif //HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH2SOLVER_H
