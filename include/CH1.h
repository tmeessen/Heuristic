//
// Created by tmeessen on 7/28/18.
//

#ifndef HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH1SOLVER_H
#define HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH1SOLVER_H

/**
 * Size of the sample on which the performance of CH1 is determined
 */
#define SAMPLE_SIZE 15

#include <vector>

#include "Solver.h"
#include "mkpsolution.h"

class CH1Solver: public Solver {
    std::vector<int> random_vector;
public:
    /**
     *  Shuffle item_available, parse it and return the first item marked as 0
     */
    int get_next_candidate(int const*items_picked, problem* p, int const *items_ignored) override;
    Solution* solve(problem *) override ;
};

class CH1Solution: public Solution {
    int average_profit;
public:
    CH1Solution(problem *p, int average_profit);
    void print()const override;
    int get_profit()const { return average_profit;};
};


#endif //HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH1SOLVER_H
