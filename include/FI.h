//
// Created by tmeessen on 7/31/18.
//

#ifndef HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_FI_H
#define HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_FI_H

#include "Solver.h"

class FI : public Solver {
    Solver *initial_solver;
    Solution *do_a_move(Solution *s, problem *p, int *items_to_remove, int k);
    /**
     * Encapsulate the solver that have been setup
     */
    int get_next_candidate(int const* items_picked, problem* p, int const *items_ignored) override { return initial_solver->get_next_candidate(items_picked,p, items_ignored);}
public:
    explicit FI(Solver *s);
    Solution *solve(problem *p) override;
};


#endif //HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_FI_H
