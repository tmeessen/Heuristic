//
// Created by tmeessen on 7/28/18.
//

#ifndef HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH3_H
#define HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH3_H

#include "Solver.h"
#include <vector>

class CH3Solver: public Solver {
private:

    /**
     * Give the item with the maximum first-iteration's pseudo-utility.
     * Pseudo-utility:
     *  Vj = Sum_i(a_ij) i-constraint j-items
     *  utility = profit_j / Vj
     * @param n number of item
     * @param m number of constraints
     * @param profits item's profit
     * @param constraint constraints's matrix normalized so that (sum(constraint) == 1)
     * @return Item
     */
    int get_first(int n, int m, int * profits, float **constraint);
    /**
     * Give the item with the maximum Toyoda's pseudo-utility corresponding to the item-set state.
     * Pseudo-utility:
     *  Ui = sum_j(a_ij * item_j)
     *  U'i = Ui/norm(U)
     *  Vj = sum_i(a_ij * U'i)
     *  utility = profit_j / Vj
     * @param n number of item
     * @param m number of constraints
     * @param profits item's profit
     * @param constraint constraints's matrix normalized so that (sum(constraint) == 1)
     * @param items vector marking already selected item by 1, 0 otherwise.
     * @return best item
     */
    int get_best(int n, int m, int * profits, float **constraint, int const *items, int const *ignored);
    /**
     * return the normalized matrix of constraints for a specific problem
     * @param n number of item
     * @param m number of constraints
     * @param profits item's profit
     * @param constraint constraints's matrix
     * @param capacities capacities of the original mkp problem
     * @return normalized constraint matrix
     */
    float **norm_constraints(int n, int m, int **constraints, int *capacities);

public:
    int get_next_candidate(int const* items_picked, problem* p, int const *items_ignored) override;

    Solution* solve(problem *p) override;

};



#endif //HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CH3_H
