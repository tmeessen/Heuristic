/***************************************************************************
 *   Copyright (C) 2018 Alberto Franzin                                    *
 *   alberto.franzin@ulb.ac.be                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __MKPSOLUTION_H__
#define __MKPSOLUTION_H__


#include <string>

#include "mkpdata.h"
#include "mkpproblem.h"

class Solution {
public:
    int n;
    int m;
    int value;
    int *sol;

    int num_selected;
    int num_discarded;

    int *resources_used;
    int *violated;
    int num_violated;

    std::string solver_desc;

    Solution();
    Solution(problem *p );

    Solution (const Solution&);

    virtual void print() const;

    // Liberate memory
    ~Solution();

    // add item to the solution without feasibility checks
    void add_item(int item, problem *p);

    /*
     * check whether adding item would lead to some constraints being violated
     *
     * if yes, don't add, and return the number of constraints that
     * would be violated by adding item
     *
     * if no, add and return 0
     */
    int check_and_add_item(int item, problem *p);

    virtual int get_profit() const { return value;}

    void remove_item(int item, problem *p);
};


#endif

