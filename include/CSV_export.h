//
// Created by tmeessen on 7/30/18.
//

#ifndef HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CSV_EXPORT_H
#define HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CSV_EXPORT_H

#include <string>

#include "util.h"
#include "mkpproblem.h"

class CSV_export {
    std::string log_file;
public:
    explicit CSV_export(char* logfile);
    void print(char *problem_name, int value, SolverName solver);
    void time_log(double time);
};


#endif //HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_CSV_EXPORT_H
