//
// Created by tmeessen on 7/27/18.
//

#ifndef HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_MKPSOLVER_H
#define HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_MKPSOLVER_H


#include "mkpsolution.h"
#include "mkpproblem.h"


//
// Create a random solution adapted to the given problem
// To create more complex solver expand this class
//
class Solver
{

public:
    /*
     * Look into the current state of the solution and pick a candidate to be added next.
     * @param items_available Vector of n item, item that can be picked are marked with 0
     * @param profits Profit value of each item
     * @param constraint Constraint matrix
     * @return Candidate item
     */
    virtual int get_next_candidate(int const*items_picked, problem* p, int const *item_ignored) = 0;
    /*
     * Return a feasible Solution of the problem
     */
    virtual Solution* solve( problem *p) = 0;


    /*
     * create an empty solution for a MKP problem
     *
     * no items selected
     */
    Solution *create_empty_solution(problem *p);
};



#endif //HEURISTIC_OPTIMIZATION_IMPLEMENTATION_EXERCICE_MKPSOLVER_H
