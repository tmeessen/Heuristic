/***************************************************************************
 *   Copyright (C) 2018 Alberto Franzin                                    *
 *   alberto.franzin@ulb.ac.be                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __MKPUTIL_H__
#define __MKPUTIL_H__

#include <vector>
#include <string>

#include "Solver.h"


// create a vector of n shuffled integers (values from 0 to n-1)
int *create_shuffled(int n);

// shuffle vector of n integers
void shuffle_int(int *vector, int n);

/**
 * Algorithm type for initial solution
 */
enum class SolverName {
    CH2,
    CH1,
    CH3,
};

/**
 * Improvement algorithm type
 */
enum class Move_type {
    None,
    FI,
    BI,
};


/**
 * Build a Solver object with given configuration
 * @param solver_name Algorithm for initial solution and improvement-step evaluation
 * @param move_type Improvement strategy
 * @return s A pointer to an instantiated Solver object
 */
Solver* getSolver(SolverName, Move_type);




class Params {
    /**
     * File used to log result
     */
    char* log_file;
    /**
     * Every files containing mkp problems
     */
    std::vector<std::string> instance_files;
    /**
     * Number of file already given.
     */
    int file_read;
    /**
     * Seed used for the random generator
     */
    int seed;
    /**
     * Solver to be use for initial solution
     */
    SolverName solver;
    /**
     * Improvement algorithm
     */
    Move_type move_type;
public:
    /**
     * Read command-line parameters
     * @param argc argument count
     * @param argv argument values
     */
    Params(int argc, char *argv[]);
    /**
     * Parse to the filenames retrieved from command-line parameters.
     * If no more filename are available return nullptr.
     * @return a filename or nullptr if no more are available
     */
    char *get_next_filename();

    /**
     * Set the random seed
     */
    void set_seed() {srand(seed);}

    int get_seed() const { return seed;}

    char *get_log_file() const { return log_file;}

    SolverName get_solver_name() const { return solver;}

    Move_type get_solver_move() const { return move_type;}
};





#endif
