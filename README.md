# Heuristic Optimization

This project contains implementation and analysis of heuristic algorithms implemented for the mkp problem. 
The work is based on a base code in C given by Alberto Franzin which have been refactored and extended in C++.
Program usage for the specific required task is performed with documented Bash scripts. 
Results analysis is performed using R.

## Structure

- Report can be found split in each exercise-related folder in `report`.
- Source code can be found in `src/`.
- Headers files in `include`.
- Material describing the assignement are in `assignement`

## Build

The project use CMake as a build system.
To generate the Makefile use `cmake CMakeLists.txt`
Then simply execute it with `make`

## Execution
Once you have build the project for your configuration.
 Put the executable (named main) in the folder corresponding to the exercice of your choice.
 There you can find a script in bash to generate the data from the program and a second in R to perform the requested analysis.
 
 The Bash script will generate data files and the R script will report on stdout.

See script's documentations for more information.

## Program's manual
First two parameters are mandatory:
- `--output filename1` File that wil received the results (previous content will be discarded).
- `--files filename1 filename2 ...` List of files containing the descriptions of the problems.
- `--seed random_integer` random seed to be used.

- `--CH1`, `--CH2`, `--CH3` Solve the problem(s) using specified algorithm. 
Description can be found with the assignement. 
Using multiple will not cause an error, only the last one will be considered.

## Code architecture
Each CH* algorithm is an implementation of th parent class Solver, allowing interchangeability. 
The inspiration there was a strategy pattern.

FI and BI improvement were implemented as a decorator layer on top of the CH strategy pattern.
At this stage this architecture is did not seemed to be well suited for the CH1 algorithm as it required a specific behavior to average it's results.

Development is halted with an inoperative improvement layer due to unsolved memory heap corruption in the FI specific code.
## Report

The report is composed of a markdown report for each exercise and in-file documentation.
 
## Conclusion

The proposed work is interesting. 
It is a nice exercise to practice in programming.
Assignment description and accompanying educational material is great, with good clarity.
The proposed implementation joined with this report is sadly only at the beginning.
If, beyond the lack of resources allocated to task, a reason is needed for this little implementation. 
It should be the lack of practice with pointers and the difficulty to prevent and diagnose the frequent cases of memory heap corruption.
