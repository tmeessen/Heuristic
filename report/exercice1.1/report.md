# Exercise 1.1
## Disclaimer
No optimization were performed on build or on the code that generated the datas for this analysis.
## Results
Name | Profit(%) | Time(ms) 
---  | ---       |  ---
CH1  | 82        | 95
CH2  | 91.2      | 79
CH3  | 96.9      | 613

Wilcoxon | CH1  | CH2  | CH3
---      | ---  |  --- | ---
CH1      | ~    | x    | x
CH2      | x    | ~    | x
CH3      | x    | x    | ~

## Discussion
Each algorithm have a significant difference in their performance relative to profit as the Wilcoxon illustrate.
That conclusion were obvious when the means of each sample are compared.
Toyo pseudo-utility performed significantly better with an 6% improvement over the greedy version and 3 times more computation.

### Long execution time
Due to implementation choices priority lists are not persistent and are computed at each iteration.
It is of importance for CH1 which could have an execution time around 10 ms second without that overhead.
