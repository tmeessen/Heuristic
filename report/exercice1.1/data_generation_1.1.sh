#!/bin/bash
# Launch the program for every instances present in ${directory} and for each CH algorithm. No argument.
# Redirect any remaining debug message to the file ${log}

# Project's executable
executable="main"
# Data file name. If this is change change the R report in accordance
name='exercice1.1'
# Where the instances are to be found
directory='../../mkp_instances/instances'
# Any remaining output from project's executable will be redirected here
log='log.txt'
echo "Starting CH[1-3] on problems located at $directory"

./${executable} --seed 1492 --output "${name}_CH1.dat" --files "$directory/"*.dat --CH1 > $log
echo "CH1 -- done"
./${executable} --seed 1492 --output "${name}_CH2.dat" --files "$directory/"*.dat --CH2 > $log
echo "CH2 -- done"
./${executable} --seed 1492 --output "${name}_CH3.dat" --files "$directory/"*.dat --CH3 > $log
echo "CH3 -- done"